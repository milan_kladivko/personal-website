
// Shorthands for element queries
const $ = (sel) => document.querySelectorAll(sel)
const $$ = (sel) => document.querySelector(sel)

// Flag for conditioning debugging behavior
var debug = true
// Logging function shorthand
const log = function(whatever) { console.log(whatever) }


// Remove all 'not-loaded' classes on load 
var ___Load_Effects = (function LoadFX() {

	const class_name = "not-loaded"
	
	document.addEventListener('DOMContentLoaded', () => {
		setTimeout(() => {
			$('.' + class_name).forEach((el) => {
				el.classList.remove(class_name)
			})
		}, 100)
	})
	
})()


function on_load(event) {

  window.$body = $$('body')  // @Global

  if (debug) 
    use_darkmode_keyboard_switch("Backquote")

  use_header_scroll_modes()
  
}
document.addEventListener('DOMContentLoaded', on_load)


//==========================================================
//  Darkmode
//==========================================================
function use_darkmode_keyboard_switch(keyCode) {		
	$body.addEventListener(
		'keydown',
		(event) => {
			if (event.code === keyCode)
        toggle_darkmode()
		})

  // @Incomplete; add a button in the header to switch modes

  // @Incomplete; use a client-side cookie to remember user pref mode
}


function toggle_darkmode(switch_to_dark) {
  
  let classes = $body.classList
  const LIGHT = 'light'
  const DARK = 'dark'
  
  if (switch_to_dark === true) 
    classes.replace(LIGHT, DARK)
    
  else if (switch_to_dark === false) 
    classes.replace(DARK, LIGHT)
                    
  else { 
    classes.toggle(DARK)
    classes.toggle(LIGHT)
  }
}


//==========================================================
//  Header scroller 
//==========================================================
function use_header_scroll_modes() {

  let prev_position = -1;
  let prev_diff = 0;

  let scroll_direction_handler = function(event) {
    
    let diff = event.pageY - prev_position;
    let is_scrolling_down = (diff > 0)
    let changed_direction = (Math.sign(diff) != Math.sign(prev_diff))

    // switch only when started going in a different direction
    if (changed_direction)
      switch_header_modes(is_scrolling_down)
    
    // finally update the last known scroll position
    prev_position = event.pageY;
    prev_diff = diff;
  }

  // Limit scroll response to frames (scroll event throttling)
  window.addEventListener('scroll', (event) => {
    requestAnimationFrame(() => scroll_direction_handler(event))
  })
  
}

function switch_header_modes(going_down) {

  const DOWNMODE_CLASS = 'scrolled-down' 
  let classes = window.$body.classList

  if (going_down)
    classes.add(DOWNMODE_CLASS)
  else 
    classes.remove(DOWNMODE_CLASS)
}






