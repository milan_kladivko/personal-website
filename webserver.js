
// Debug flag mainly used for object logging, they take up
// a ridiculous amount of space sometimes
const debug = false

// Flag for moments when I'm writing an article and seeing
// how it looks on the page (constant reloading)
const writing_content_right_now = true


// Use Express
const express = require('express')   // @installed
const app = express()
const port = 3000 // debug ? 3000 : 80

// Parsers (used to manually parse file contents)
const pug = require('pug')
const md_parse = require('node-markdown').Markdown

// Basic configuration
const default_dark = true
const make_title = (title, page) => {
  if (page === undefined)
    return `${title} // Milan Kladívko`
  else
    return `${title} .${page} // Milan Kladívko`
}

// Use colors for console output
var colors = require('colors')  // @installed





//---------------------------------------------------------------
//  Utilities
//---------------------------------------------------------------

let fs = require('fs')

// Use the system's way of stringifying objects
// (i.e. what console.log(o) does)
let objstr = require('util').inspect

// Better way of handling errors 
function assert_err(string, error_object) {
  if (error_object) {
    object_string = objstr(error_object)
    
    console.error(string.red.inverse)
    console.error(object_string.red)
    
    return true
  }
  else return false
}

function file_is_directory(path) {
  return fs.lstatSync(path).isDirectory()
}




//---------------------------------------------------------------
//  Page Routing Data
//---------------------------------------------------------------

// Create an empty navigation (cyclic dependency)
var _navigation = {}

// A 'Page' data type
function Static_Page(
  link_pattern,
  page_frame,
  heading, description, keywords) {

  // link used to get to the page
  this.link = link_pattern
  // pug view/template - the "type" of page
  this.frame = page_frame
  // heading used for navbar links
  this.heading = heading
  // title for SEO and browser tab 
  this.title = make_title(heading)
  // description of the page (second heading kinda)
  this.desc = description
  // keywords for SEO
  this.keys = keywords

  // nav data (home Page and others), filled later
  this.navigation = _navigation

  // a `this` reference in case I need it
  this.this_page = this
  // a default setting for dark mode
  this.dark = default_dark
};


var home_page = new Static_Page(
  '/',
  'index.pug',
  'Něco málo o mně',
  '' +
    'Na mé stránce najdete informace o mně, ' +
    'o mých zájmech a co jsem za svůj krátký život vytvořil.',
  '')
var navbar_pages = [
  new Static_Page(
    '/zdravi',
    'zdravi.pug',
    'Zdraví',
    ''+
      'Bezchybné zdraví pomocí rad, selského rozumu a zadarmo. ' +
      'Nikdo (ani doktoři) dnes nemají ponětí, ' +
      'jak zabránit moderním chronickým chorobám. ' +
      'Já vám povím v čem to všechno vězí.',
    ''),
  new Static_Page(
    '/tech',
    'tech.pug',
    'Tech',
    '' +
      '',
    ''),
  new Static_Page(
    '/projekty',
    'tech',
    'Projekty',
    '' +
      '',
    ''),
]
var static_pages = [home_page].concat(navbar_pages)

// Finally fill navigation with data
_navigation.home = home_page
_navigation.navbar = navbar_pages






//---------------------------------------------------------------
//  Express Middlewares
//  ... setting express view confs
//---------------------------------------------------------------
app.set('view engine', 'pug')  // Use PUG for `res.render()` calls
app.set('views', './views')    // Look here for PUG templates


// For styles, imgs, scripts etc. use ./public directly
app.use(express.static('./public'))
//
// ...if I ever start using TypeScript, code that here


//---------------------------------------------------------------
//  Static Routes
//  ... no extra data needed, only pug route renders
//---------------------------------------------------------------
function route_static_pages() {

  console.info(' [Started_Static_Routing]                 '
              .bgWhite.black)
  
  static_pages.forEach((page) => {
    console.info(` + ${page.link}`.green)

    // Route the page
    app.get(page.link, (req, res) => {
      
      console.info(`[Get_Static] '${req.path}' from ${req.ip} `
                 .magenta)
      // .render(<view_template>, <data_sent_to_view>)
      res.render(page.frame, page)
    })
  })
  
}
route_static_pages()


//---------------------------------------------------------------
//  Article Routes
//---------------------------------------------------------------

// Article folder containing all article data
const ARTICLE_FOLDER = 'views/health-articles/'
const DEFAULT_ARTICLES_TITLE = "Zdraví"

function route_article_pages() {

  // @Warning: beware the async file reads here 
  let articles = get_article_data_from(ARTICLE_FOLDER)

  add_article_route(articles)
  
}

function get_article_data_from(folder, articles) {

  // Start with an empty article object
  // Every page will get its own field
  // Page (...cz/zdravi/****) is the field's key
  let article_data = {}

  //
  // Fill up the data by reading the files in the folder
  //
  
  // @Warning: This filling uses asynchronous file reads,
  //   the data won't be there on this func's return!!
  
  fs.readdir(folder, (err, files) => {
    
    // if error, print error and don't bother routing at all
    if (assert_err(
      `[Error] '${folder} :: Directory error!'`, err))
      return
    
    // Go through files in the folder
    files.forEach(
      file => process_file(folder, file, article_data))
    
  })

  // Finally give back the data
  // (only queued to be filled, see @Warning)
  return article_data
}


function process_file(folder, file, articles) {

  
  // File name and path variables
  let path = folder + file
  let splits = file.split('.')
  let name = splits[0]
  
  // Skip directories
  if (file_is_directory(path)) return 
  
  // Map of file extensions to handlers
  let handlers = {
    '.pug': remember_pug_file,
    '.md': parse_markdown_content,
    '.json': read_data_from_json
  }
  let supported_extensions = Object.keys(handlers)

  // Handle the files  
  supported_extensions.forEach(extension => {

    if (file.endsWith(extension)) {
      
      fs.readFile(path, "utf-8", (err, file_data) => {
        
        let failed_to_read_file = assert_err(
          `[Error] '${file}' :: File read error!`, err)
        if (failed_to_read_file) return
        
        
        // Make a new article page object if not present
        if (articles[name] === undefined) {
          // TODO make a make_default_article() func
          articles[name] = {
            frame: 'articles.pug',
            navigation: _navigation,
            dark: default_dark
          }
        }
        
        // Process the file with mapped handler
        let handle_file = handlers[extension]
        handle_file(file_data, articles[name], path)
        
      })
    }       
  })
}


//
// File handlers - what to do with various files
//

function remember_pug_file(pug_text, article, filepath) {

  article.content_file = filepath
  
  console.info(` + '${filepath}' :: Remembering PUG file`
               .green)
}

function read_data_from_json(json_text, article, filepath) {
  
  // Parse data into an object
  json = JSON.parse(json_text)
  
  // Make a valid <title>, if present
  if (json.article_title !== undefined)
    json.title = make_title(json.article_title)
  else
    json.title = make_title(DEFAULT_ARTICLES_TITLE)
  
  // copy the loaded data to the article's data
  for (var field in json)
    article[field] = json[field]
  
  //-----------------------------------------------
  console.info(` - '${filepath}' :: Parsed JSON metadata`
               .yellow)
  if (debug)
    console.log(objstr(json).gray)
}

function parse_markdown_content(md_text, article, filepath) {
  
  // TODO consider a try block?? 
  article.content_html = md_parse(md_text)
  
  //------------------------------------------------
  console.info(`  + '${filepath}' :: [OK] Markdown parse`
               .green)
}

function parse_pug_content(pug_text, article, filepath) {

  // replace all underscores (_) with a `&nbsp;'
  pug_text = pug_text.replace(/_/gm, '&nbsp;')
  
  // try parsing the pug
  try {
    
    article.content_html = pug.render(pug_text)
    console.info(`  + '${filepath}' :: [OK] Re-parsed Pug`
                 .green)
    
  } catch (e) {
    // error while parsing
    console.error(`  - '${filepath}' :: --ERR-- Jade/Pug parse`
                  .red.inverse)
    console.error(e)
    article.content_html = '<h1>Chybka!!</h1>'
    return false
  }
  
  return true
}

// End of file handlers



function add_article_route(all_articles) {
  
  app.get('/zdravi/:article_name', (req, res, next) => {
    
    console.info(`[Get_Article] '${req.path}' - ${req.ip} `
                 .magenta)
    
    // Get article name based on part of the URL
    let name = req.params.article_name
    let article_data = all_articles[name]
    
    // Jump to 404 if we don't have
    // the article data to this page
    if (article_data === undefined) {
      next() 
      return
    }
    
    // Get path to pug content, read its text
    let filepath = article_data.content_file
    let pug_text = fs.readFileSync(filepath, "utf-8")
    // error with fileread
    if (pug_text == "") {
      console.error(`[Error] '${file}' :: File read error!`
                    .red.inverse)
      console.error(objstr(err).red)
      return
    }
    
    //
    // Parse the read text
    //
    parse_pug_content(pug_text, article_data, filepath)
    
    // 
    // Render the page with article data
    // 
    res.render(article_data.frame, article_data)
    
    if (debug) {
      console.info(` :: Article data (${name}):`.green)
      console.log(objstr(article_data).gray)
    }
  })
  
}



//
// Route all the article pages
//
route_article_pages()



//---------------------------------------------------------------
//  Error Routes
//  ... must be the last express routes as fallbacks
//---------------------------------------------------------------

// The 404 route
// Must ALWAYS be after all valid routes!!!
app.use('*', (req, res) => {

  let url = req.originalUrl
  let ip  = req.ip

  console.info(` [Response 404 Error] '${url}' - ${ip}: `
              .red)
  
  res.status(404).render(
    'error-404', {
      dark: default_dark,
      path: req.originalUrl,
    }
  )
})


// Server error 500 route
// Must ALWAYS be after ALL ROUTES, even after 404
app.use((err, req, res, next) => {
  
  /// TODO log the error later
  console.error(` [Response 500 Error]: `.red.inverse)
  console.error(err)

  res.status(500).render(
    'error-500', {
      dark: default_dark,
      error: err,
      debug: debug,
    }
  )
})



//---------------------------------------------------------------
//  Server Listen Command
//---------------------------------------------------------------
const server = app.listen(port, (err) => {
    
  if (err) {
    console.error(`[Fatal_Server_Error]`.red.inverse)
    console.error(objstr(err).red)
    return false
  }
    
  console.info("[Listen_Started]                          "
               .inverse)
  console.info(`Server listening @port :${port}\n`)
  
})

